package com.alecgdouglas.gbenthusiast;

import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.Row;
import android.util.SparseArray;

import com.alecgdouglas.gbenthusiast.model.Video;

import java.util.List;

public class VideoPositionRowUpdateUtils {

    private VideoPositionRowUpdateUtils() {
    }

    public static boolean updateRows(Video updatedVideo,
                                     SparseArray<CursorObjectAdapter> videoCursorAdapters,
                                     ArrayObjectAdapter videoRowAdapter,
                                     List<Integer> alwaysUpdateIds,
                                     List<String> alwaysUpdateHeaderNames) {

        if (updatedVideo == null || videoCursorAdapters == null) return false;

        boolean notify = false;
        for (Integer id : alwaysUpdateIds) {
            final CursorObjectAdapter alwaysUpdateAdapter = videoCursorAdapters.get(id);
            if (alwaysUpdateAdapter != null) {
                notify |= updateVideoInAdapter(updatedVideo, alwaysUpdateAdapter);
            }
        }

        final List<String> videoTypeNames = updatedVideo.getVideoTypeNames();
        for (String videoTypeName : videoTypeNames) {
            final CursorObjectAdapter typeRowAdapter =
                    videoCursorAdapters.get(videoTypeName.hashCode());
            if (typeRowAdapter != null) {
                notify |= updateVideoInAdapter(updatedVideo, typeRowAdapter);
            }
        }
        if (notify) {
            for (int i = 0; i < videoRowAdapter.size(); i++) {
                final Row row = (Row) videoRowAdapter.get(i);
                if (row == null) continue;

                final HeaderItem header = row.getHeaderItem();
                if (header == null) continue;
                if (videoTypeNames.contains(header.getName()) ||
                        alwaysUpdateHeaderNames.contains(header.getName())) {
                    videoRowAdapter.notifyArrayItemRangeChanged(i, 1);
                }
            }
        }
        return notify;
    }

    private static boolean updateVideoInAdapter(Video updatedVideo, CursorObjectAdapter adapter) {
        for (int i = 0; i < adapter.size(); i++) {
            final Video video = (Video) adapter.get(i);
            if (video.getId() == updatedVideo.getId()) {
                video.setPositionMillis(updatedVideo.getPositionMillis());
                // Assume a video only shows up once in any given row.
                return true;
            }
        }
        return false;
    }
}
