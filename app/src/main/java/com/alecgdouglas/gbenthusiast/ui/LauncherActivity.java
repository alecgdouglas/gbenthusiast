package com.alecgdouglas.gbenthusiast.ui;

import android.content.Intent;
import android.os.Bundle;

import com.alecgdouglas.gbenthusiast.ApiKeyManager;

public class LauncherActivity extends CommonActivity {
    private static final int LOGIN_REQUEST_CODE_REDIRECT_TO_MAIN = 1;
    private static final int LOGIN_REQUEST_CODE_REDIRECT_TO_SEARCH = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ApiKeyManager.hasApiKey(this)) {
            startActivityFresh(MainActivity.class);
        } else {
            attemptLogin(LOGIN_REQUEST_CODE_REDIRECT_TO_MAIN);
        }
    }

    @Override
    public boolean onSearchRequested() {
        if (ApiKeyManager.hasApiKey(this)) {
            super.onSearchRequested();
        } else {
            attemptLogin(LOGIN_REQUEST_CODE_REDIRECT_TO_SEARCH);
        }
        return true;
    }

    private void attemptLogin(int requestCode) {
        startActivityForResult(new Intent(this, LoginActivity.class), requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == LOGIN_REQUEST_CODE_REDIRECT_TO_MAIN) {
                startActivityFresh(MainActivity.class);
            } else if (requestCode == LOGIN_REQUEST_CODE_REDIRECT_TO_SEARCH) {
                startActivityFresh(SearchActivity.class);
            } else {
                finishAfterTransition();
            }
        } else {
            finishAfterTransition();
        }
    }

    private void startActivityFresh(Class activityClass) {
        final Intent intent = new Intent(this, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(intent);
        finishAfterTransition();
    }
}
