package com.alecgdouglas.gbenthusiast.ui;

import android.support.v17.leanback.widget.HeaderItem;

import static android.support.v17.leanback.widget.ObjectAdapter.NO_ID;

public class IconHeaderItem extends HeaderItem {
    public static final int NO_ICON_ID = -1;

    private final int mIconDrawableResourceId;

    public IconHeaderItem(long id, String name, int iconDrawableResourceId) {
        super(id, name);
        mIconDrawableResourceId = iconDrawableResourceId;
    }

    public IconHeaderItem(long id, String name) {
        this(id, name, NO_ICON_ID);
    }

    public IconHeaderItem(String name) {
        this(NO_ID, name);
    }

    public IconHeaderItem(String name, int iconDrawableResourceId) {
        this(NO_ID, name, iconDrawableResourceId);
    }

    public int getIconDrawableResourceId() {
        return mIconDrawableResourceId;
    }
}
