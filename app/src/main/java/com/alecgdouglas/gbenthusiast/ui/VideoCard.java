package com.alecgdouglas.gbenthusiast.ui;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v17.leanback.widget.ImageCardView;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.alecgdouglas.gbenthusiast.presenter.DrawableItem;
import com.bumptech.glide.Glide;

public class VideoCard implements DrawableItem {

    private final String mProgressTextTag = "progress_text";
    private final String mDurationTextTag = "duration_text";

    @Override
    public void drawItem(ImageCardView cardView, Object item) {
        Video video = (Video) item;
        cardView.setTitleText(video.getName());
        cardView.setContentText(video.getFormattedPublishDate());

        if (video.getMediumImageUrl() != null) {
            Glide.with(cardView.getContext()).load(video.getMediumImageUrl()).centerCrop()
                    .error(R.drawable.error_thumbnail).into(cardView.getMainImageView());
        }

        final TextView progressText = (TextView) cardView.findViewWithTag(mProgressTextTag);
        if (!video.isLive()) {
            if (video.getProgress() == 1f) {
                final ColorMatrix desatMatrix = new ColorMatrix();
                desatMatrix.setSaturation(0);
                cardView.getMainImageView().setColorFilter(new ColorMatrixColorFilter(desatMatrix));
            } else {
                // If we don't reset the color matrix, then other videos may end up using it even if
                // their progress isn't 100%.
                cardView.getMainImageView().setColorFilter(null);
            }

            final TextView durationText = (TextView) cardView.findViewWithTag(mDurationTextTag);
            durationText.setText(video.getFormattedDuration());

            progressText.setText(video.getFormattedProgressPercent());
        } else {
            progressText.setText(R.string.live_progress);
        }
    }
}
