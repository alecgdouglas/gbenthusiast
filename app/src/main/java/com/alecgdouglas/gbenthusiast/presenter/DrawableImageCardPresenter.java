package com.alecgdouglas.gbenthusiast.presenter;

import android.support.v17.leanback.widget.ImageCardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.ui.DrawableGridItem;

public class DrawableImageCardPresenter extends CardPresenter {
    public DrawableImageCardPresenter() {
        super(CardSize.SMALL);
    }

    @Override
    public void onBindImageCardView(ImageCardView cardView, Object item) {
        ((DrawableItem) item).drawItem(cardView, item);
    }
}
