package com.alecgdouglas.gbenthusiast.model;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v17.leanback.widget.ImageCardView;
import android.util.Log;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.VideoUrlFormatter;
import com.alecgdouglas.gbenthusiast.presenter.DrawableItem;
import com.bumptech.glide.Glide;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Video implements Serializable {
    public static final String NULL_VIDEO_URL = "null";
    public static final String VIDEO_TYPE_DELIMITER = ",";
    private static final String TAG = "Video";
    private static final DateFormat sDateFormat =
            new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
    private static final long serialVersionUID = 1L;
    private static final String VIDEO_TYPE_DELIMITER_REGEX = "\\s*" + VIDEO_TYPE_DELIMITER + "\\s*";
    private final String mApiKey;
    private final long mId;
    private final String mName;
    private final String mDeck;
    private final String mVideoTypeNames;
    private final Date mPublishDate;
    private final String mLowUrl;
    private final String mHighUrl;
    private final String mHdUrl;
    private final long mLengthSeconds;
    private final String mTinyImageUrl;
    private final String mMediumImageUrl;
    private final String mSuperImageUrl;
    private final String mIconImageUrl;
    private final boolean mIsLive;
    private long mPositionMillis;

    private Video(String apiKey, long id, String name, String deck, String videoTypeName,
                  Date publishDate, long lengthSeconds, String lowUrl, String highUrl, String hdUrl,
                  String tinyImageUrl, String mediumImageUrl, String superImageUrl,
                  String iconImageUrl, long positionMillis, boolean isLive) {
        mApiKey = apiKey;
        mId = id;
        mName = name;
        mDeck = deck;
        mVideoTypeNames = videoTypeName;
        mPublishDate = publishDate;
        mLengthSeconds = lengthSeconds;
        mLowUrl = lowUrl;
        mHighUrl = highUrl;
        mHdUrl = hdUrl;
        mTinyImageUrl = tinyImageUrl;
        mMediumImageUrl = mediumImageUrl;
        mSuperImageUrl = superImageUrl;
        mIconImageUrl = iconImageUrl;
        mPositionMillis = positionMillis;
        mIsLive = isLive;
    }

    public static DateFormat getDateFormat() {
        return sDateFormat;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getDeck() {
        return mDeck;
    }

    public Date getPublishDate() {
        return mPublishDate;
    }

    public String getMediumImageUrl() {
        return mMediumImageUrl;
    }

    public String getSuperImageUrl() {
        return mSuperImageUrl;
    }

    public String getVideoUrlForQuality(VideoQuality quality) {
        if (!hasUrlForQuality(quality)) {
            if (quality.equals(VideoQuality.LOW)) {
                // Can't step down any further, try finding a URL for any video quality at all.
                return getFirstNonNullVideoUrl();
            }
            return getVideoUrlForQuality(VideoQuality.stepDown(quality));
        }
        switch (quality) {
            case HD:
                return getHdUrl();
            case HIGH:
                return getHighUrl();
            case LOW:
                return getLowUrl();
        }
        Log.e(TAG, "Unhandled VideoQuality value: " + quality.toString());
        return getFirstNonNullVideoUrl();
    }

    /**
     * @return The URL for the first video quality that we have a URL, or null if we have no URLs.
     */
    private String getFirstNonNullVideoUrl() {
        for (VideoQuality quality : VideoQuality.values()) {
            if (hasUrlForQuality(quality)) {
                return getVideoUrlForQuality(quality);
            }
        }
        Log.w(TAG, "There are no URLs for any video quality for video '" + getName() + "'");
        return null;
    }

    public boolean hasUrlForQuality(VideoQuality quality) {
        switch (quality) {
            case HD:
                return doesUrlExist(mHdUrl);
            case HIGH:
                return doesUrlExist(mHighUrl);
            case LOW:
                return doesUrlExist(mLowUrl);
            default:
                return false;
        }
    }

    private boolean doesUrlExist(String url) {
        return url != null && !url.isEmpty() && !url.equals(NULL_VIDEO_URL);
    }

    public String getFormattedPublishDate() {
        if (getPublishDate() == null) return "";
        final DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(getPublishDate());
    }

    public long getDurationMillis() {
        return TimeUnit.SECONDS.toMillis(mLengthSeconds);
    }

    public String getFormattedDuration() {
        PeriodFormatter formatter =
                new PeriodFormatterBuilder().appendDays().appendSuffix("d").appendSeparator("")
                        .appendHours().appendSuffix("h").appendSeparator("").appendMinutes()
                        .appendSuffix("m").appendSeparator("").appendSeconds().appendSuffix("s")
                        .toFormatter();
        final boolean tooLarge =
                mLengthSeconds > Integer.MAX_VALUE || mLengthSeconds < Integer.MIN_VALUE;
        final int length = (tooLarge) ? Integer.MAX_VALUE : (int) mLengthSeconds;
        final Period period = Period.seconds(length).normalizedStandard();
        return formatter.print(period);
    }

    private String getHdUrl() {
        return VideoUrlFormatter.formatUrl(mHdUrl, mApiKey, isLive());
    }

    private String getHighUrl() {
        return VideoUrlFormatter.formatUrl(mHighUrl, mApiKey, isLive());
    }

    private String getLowUrl() {
        return VideoUrlFormatter.formatUrl(mLowUrl, mApiKey, isLive());
    }

    public List<String> getVideoTypeNames() {
        if (mVideoTypeNames == null) return Collections.emptyList();
        return Arrays.asList(mVideoTypeNames.trim().split(VIDEO_TYPE_DELIMITER_REGEX));
    }

    public long getPositionMillis() {
        return mPositionMillis;
    }

    public void setPositionMillis(long positionMillis) {
        mPositionMillis = positionMillis;
    }

    /**
     * @return The users progress through the video between 0 and 1, 0 corresponding to 0% and 1
     * corresponding to 100%.
     */
    public float getProgress() {
        if (mPositionMillis <= 0 || mLengthSeconds <= 0) {
            return 0f;
        } else {
            return Math.max(0f, Math.min(1f,
                    (float) mPositionMillis / TimeUnit.SECONDS.toMillis(mLengthSeconds)));
        }
    }

    public String getFormattedProgressPercent() {
        return String.format("%.1f", (100 * getProgress())) + "%";
    }

    public boolean isLive() {
        return mIsLive;
    }

    @Override
    public String toString() {
        return "Video{" + "mId=" + mId + ", mName='" + mName + '\'' + ", mDeck='" + mDeck + '\'' +
                ", mVideoTypeNames=" + mVideoTypeNames + ", mPublishDate=" + mPublishDate + ", " +
                "" + "" + "mLengthSeconds=" + mLengthSeconds + ", mLowUrl=" + mLowUrl + ", " +
                "mHighUrl=" + mHighUrl + ", mHdUrl=" + mHdUrl + ", mTinyImageUrl=" + mTinyImageUrl +
                ", " + "mMediumImageUrl=" + mMediumImageUrl + ", mSuperImageUrl=" + mSuperImageUrl +
                ", " + "" + "mIconImageUrl=" + mIconImageUrl + ", " + "mPositionMillis =" +
                mPositionMillis + '}';
    }

    public static class Builder {
        private final String mApiKey;
        private final long mId;
        private String mName;
        private String mDeck;
        private String mVideoTypeName;
        private Date mPublishDate;
        private long mLengthSeconds;
        private String mLowUrl;
        private String mHighUrl;
        private String mHdUrl;
        private String mTinyImageUrl;
        private String mMediumImageUrl;
        private String mSuperImageUrl;
        private String mIconImageUrl;
        private Long mPositionMillis = 0L;
        private boolean mIsLive = false;

        public Builder(String apiKey, long id) {
            mApiKey = apiKey;
            mId = id;
        }

        public Builder(String apiKey, long id, String name) {
            this(apiKey, id);
            mName = name;
        }

        public Video build() {
            return new Video(mApiKey, mId, mName, mDeck, mVideoTypeName, mPublishDate,
                    mLengthSeconds, mLowUrl, mHighUrl, mHdUrl, mTinyImageUrl, mMediumImageUrl,
                    mSuperImageUrl, mIconImageUrl, mPositionMillis, mIsLive);
        }

        public Builder withName(String name) {
            mName = name;
            return this;
        }

        public Builder withDeck(String deck) {
            mDeck = deck;
            return this;
        }

        public Builder withVideoTypeName(String videoTypeName) {
            mVideoTypeName = videoTypeName;
            return this;
        }

        public Builder withPublishDate(Date publishDate) {
            mPublishDate = publishDate;
            return this;
        }

        public Builder withLengthSeconds(long lengthSeconds) {
            mLengthSeconds = lengthSeconds;
            return this;
        }

        public Builder withLowUrl(String lowUrl) {
            mLowUrl = lowUrl;
            return this;
        }

        public Builder withHighUrl(String highUrl) {
            mHighUrl = highUrl;
            return this;
        }

        public Builder withHdUrl(String hdUrl) {
            mHdUrl = hdUrl;
            return this;
        }

        public Builder withTinyImageUrl(String tinyImageUrl) {
            mTinyImageUrl = tinyImageUrl;
            return this;
        }

        public Builder withMediumImageUrl(String mediumImageUrl) {
            mMediumImageUrl = mediumImageUrl;
            return this;
        }

        public Builder withSuperImageUrl(String superImageUrl) {
            mSuperImageUrl = superImageUrl;
            return this;
        }

        public Builder withIconImageUrl(String iconImageUrl) {
            mIconImageUrl = iconImageUrl;
            return this;
        }

        public Builder withPositionMillis(long positionMillis) {
            mPositionMillis = positionMillis;
            return this;
        }

        public Builder setIsLive(boolean isLive) {
            mIsLive = isLive;
            return this;
        }
    }
}
